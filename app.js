const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const mailgun = require('mailgun-js');
const path = require('path');

const port = 3000;
const app = express();

// APP SETTINGS
app.use(express.static('build'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true, }));
app.use(compression());
app.disable('x-powered-by');

// ROUTES
app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, '/build/index.html'), function (err) {
        if (err) { res.status(500).send(err); }
    });
});

// #region MAILGUN

//Your api key, from Mailgun’s Control Panel
const apiKey = '917e60a46d4d7b9acb33fb790d3b9228-898ca80e-81b3224a';

//Your domain, from the Mailgun Control Panel
const domain = 'sandboxe8c923ca3d044ee2b9b93832c5b28f3a.mailgun.org';

//Your email addresses
const from_who = 'gabedeyo@gmail.com';
const to_whom = 'gabedeyo@gmail.com';

app.post('/mailtest', function (req, res) {
    // We pass the api_key and domain to the wrapper, or it won't be able to identify / send emails
    // NOTE: Mailgun is unverified under the IBI Network; Test on guest
    const mail = new mailgun({
        apiKey,
        domain,
    });

    const data = {
        from: from_who,
        to: to_whom,
        subject: 'Email Test',
        html: `<div>
            <h1>Mailgun Test</h1>
            <h2>I am a test from http://localhost:${port}</h2>
        </div>`,
    };

    mail.messages().send(data, function (err, body) {
        if (err) return res.json({ success: true, message: 'faild to send mail', err });
        return res.json({ success: true, body, });
    });
});

// #endregion

// Serve the files on port 3000
app.listen(port, err => {
    if (err) console.error(err);
    else console.log(`App listening on port http://localhost:${port}/\n`);
});
