import React, { Component } from 'react';
import './Error404.scss';

export default class Error404 extends Component {
    render() {
        return (
            <div className="Error404">
                404 - Page Not Found
                <br />
                <a href='/'
                    onClick={e => {
                        e.preventDefault();
                        window.history.back();
                    }}
                >Back</a>
            </div>
        );
    }
}