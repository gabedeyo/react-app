import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Landing.scss';

export default class Landing extends Component {
    componentDidMount() {
        console.log('Landing Mount');
    }

    render() {
        return (
            <div className="Landing">
                Landing Page
                <br />
                <Link to="/page2">Page 2</Link>
            </div>
        );
    }
}
