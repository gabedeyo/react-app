import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Page2.scss';

export default class Page2 extends Component {
    componentDidMount() {
        console.log('Page 2 Mount');
    }

    render() {
        return (
            <div className="Page2">
                Page 2
                <br />
                <Link to="/">Home</Link>
            </div>
        );
    }
}
