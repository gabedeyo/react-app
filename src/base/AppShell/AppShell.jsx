import React, { Component } from 'react';
import './AppShell.scss';

/* Use image as background
    import Background from './background.jgp';
    const style = {
        backgroundImage: `url(${Background})`,
        backgroundRepeat: 'repeat',
        backgroundPosition: 'left top',
        backgroundAttachment: 'scroll',
    };
*/

export default class AppShell extends Component {
    render() {
        return (
            <div className="AppShell">
                {this.props.children}
            </div>
        );
    }
}
