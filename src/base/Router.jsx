import React, { Component, Suspense } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// Layout
import AppShell from './AppShell/AppShell.jsx';

// Pages
const Landing = React.lazy(() => import('../pages/Landing/Landing.jsx'));
const Page2 = React.lazy(() => import('../pages/Page2/Page2.jsx'));
const Error404 = React.lazy(() => import ('../components/Error404/Error404.jsx'));

export default class Router extends Component {
    render() {
        return (
            <BrowserRouter className="Router">
                <AppShell>
                    {/* App Shell Components; i.e. navigation, sidebar */}

                    {/* Sandbox */}
                    <Switch>

                        <Route exact path="/" component={() => (
                            <Suspense fallback={<div>Loading Landing Page...</div>}>
                                <Landing />
                            </Suspense>
                        )} />

                        <Route exact path="/page2" component={() => (
                            <Suspense fallback={<div>Loading Page 2...</div>}>
                                <Page2 />
                            </Suspense>
                        )} />

                        {/* Error - 404 */}
                        <Suspense fallback={<div>404</div>}>
                            <Route component={() => <Error404 />} />
                        </Suspense>

                    </Switch>

                    {/* <Suspense fallback={<div>Footer</div>}><Footer /></Suspense> */}

                </AppShell>
            </BrowserRouter>
        );
    }
}
